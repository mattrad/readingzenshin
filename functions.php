<?php

/* Child theme text domain */
load_theme_textdomain('zenshin');

/**
 * Load the stylesheet from the parent theme with theme version added automatically.
 * https://ulrich.pogson.ch/how-to-load-the-parent-styles-in-child-themes
 */
function mattrad_parent_styles() {

	$parent = get_template();
	$parent = wp_get_theme( $parent );

	// Enqueue the parent stylesheet
	wp_enqueue_style( '18tags', get_template_directory_uri() . '/style.css', array(), $parent['Version'], 'all' );

}
add_action( 'wp_enqueue_scripts', 'mattrad_parent_styles' );

/* Pretty up the login */
function mattrad_login_css() {
echo '<link rel="stylesheet" type="text/css" href="'.get_stylesheet_directory_uri().'/css/login-styles.css">';
}
add_action('login_head', 'mattrad_login_css');
